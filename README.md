# ddft.wiki

Source code for ddft.wiki

## To build

```bash

lein run

```

## To test locally

```bash

lein ring server

```

